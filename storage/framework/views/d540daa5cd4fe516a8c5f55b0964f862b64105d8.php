<div class="section-top-border">
    <h3 class="mb-30">Liste des livres</h3>
    <div class="progress-table-wrap">
        <div class="progress-table">
            <div class="table-head">
                <div class="country">Livre</div>
                <div class="visit">Auteur</div>
                <div class="visit">Prix</div>
                <div class="percentage">Actions</div>
            </div>
            <?php $__currentLoopData = $books; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $book): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="table-row">
                <div class="country"> <img src="<?php echo e($book->cover); ?>" style="width:
                8.5vw;" alt="cover"><?php echo e($book->designation); ?></div>
                <div class="visit"><?php echo e($book->auteur); ?></div>
                <div class="visit"><?php echo e($book->prix); ?></div>
                <div class="percentage">
                    <div class="progress" style="height: 5vh;">
                        <a href="book/<?php echo e($book->id); ?>/edit" class="genric-btn
                        primary circle arrow">Modifier<span class="lnr lnr-arrow-right"></span></a>
                        <form action="<?php echo e(route('book.destroy', $book->id)); ?>" method="POST">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('delete'); ?>
                            <input type="submit" value="Supprimer" class="genric-btn danger circle arrow">
                        </form>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
</div>
<?php /**PATH C:\laragon\www\LaravelMiddleware\resources\views/book/index.blade.php ENDPATH**/ ?>